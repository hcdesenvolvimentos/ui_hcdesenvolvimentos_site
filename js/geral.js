$(function(){

	/*****************************************
	*           CARROSSEIS                   *
	*****************************************/

	
	//CARROSSEL DE DESTAQUE
    $("#carrosselDestaque").owlCarousel({
        items : 1,
        dots: true,
        loop: false,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag  : true,         
        autoplayTimeout:5000,
        autoplayHoverPause:true,
        smartSpeed: 450,

        //CARROSSEL RESPONSIVO
        //responsiveClass:true,             
        /*responsive:{
            320:{
                items:1
            },
            600:{
                items:2
            },
           
            991:{
                items:2
            },
            1024:{
                items:3
            },
            1440:{
                items:4
            },
                                    
        }*/                             
        
    });
    //BOTÕES DO CARROSSEL ESTANTE
    var carrossel_destaque = $("#carrosselDestaque").data('owlCarousel');
    $('.carrosselDestaqueEsquerda').click(function(){ carrossel_destaque.prev(); });
    $('.carrosselDestaqueDireita').click(function(){ carrossel_destaque.next(); });

    
    //CARROSSEL DE DEPOIMENTOS
    $("#carrossleOfTestimonials").owlCarousel({
		items : 7,
        dots: true,
        loop: true,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag  : true,	       
	    // autoplayTimeout:5000,
	    autoplayHoverPause:true,
        autoplay:true,
        autoplayTimeout:31,
        smartSpeed:7500,
        center:true,

	    //CARROSSEL RESPONSIVO
	    responsiveClass:true,			    
        responsive:{
            1:{
                items:1
            },
            // 600:{
            //     items:2
            // },
            768:{
                items:3
            },
            991:{
                items:4
            },
            1100:{
                items:4
            },
            1367:{
                items:6
            },
            1600:{
                items:7
            },
            			            
        }		    		   		    
	    
	});

    //CARROSSEL DE PROJETOS
    $("#carouselOfProjects").owlCarousel({
        items : 1,
        dots: true,
        loop: false,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag  : true,         
        autoplayTimeout:5000,
        autoplayHoverPause:true,
        smartSpeed: 450,

        //CARROSSEL RESPONSIVO
        //responsiveClass:true,             
        /*responsive:{
            320:{
                items:1
            },
            600:{
                items:2
            },
           
            991:{
                items:2
            },
            1024:{
                items:3
            },
            1440:{
                items:4
            },
                                    
        }*/                             
        
    });

     //CARROSSEL DE PROJETOS
    $("#carouselblog").owlCarousel({
        items : 3,
        dots: true,
        loop: false,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag  : true,         
        autoplayTimeout:5000,
        autoplayHoverPause:true,
        smartSpeed: 450,

        //CARROSSEL RESPONSIVO
        //responsiveClass:true,             
        /*responsive:{
            320:{
                items:1
            },
            600:{
                items:2
            },
           
            991:{
                items:2
            },
            1024:{
                items:3
            },
            1440:{
                items:4
            },
                                    
        }*/                             
        
    });
    //BOTÕES DO CARROSSEL ESTANTE
    var carouselblog = $("#carouselblog").data('owlCarousel');
    $('#btnLeft').click(function(){ carouselblog.prev(); });
    $('#btnRight').click(function(){ carouselblog.next(); });


    $.fn.isOnScreen = function(){
            var win = $(window);
            var viewport = {
                top : win.scrollTop(),
                left : win.scrollLeft()
            };

            viewport.right = viewport.left + win.width();
            viewport.bottom = viewport.top + win.height();

            var bounds = this.offset();
            bounds.right = bounds.left + this.outerWidth();
            bounds.bottom = bounds.top + this.outerHeight();

            return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
        };

        var num = 2;
        if (num % 2 == 0) {
            $( '.pg-inicial .sessaoInicial' ).css( "background-position", "50% 0%" );
            $('.pg-inicial .sessaoInicial').animate({
                'background-position-x': '50%',
                'background-position-y': '50%'
            }, 9000, 'linear');
            num++;
        }else{
            $( '.pg-inicial .sessaoInicial' ).css( "background-position", "50% 50%" );
            $('.pg-inicial .sessaoInicial').animate({
                'background-position-x': '50%',
                'background-position-y': '0%'
            }, 9000, 'linear');
            num++;
        }

});
